import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chess/features/event_bus.dart';
import 'package:flutter_chess/settings_page_view/settings_state.dart';

import '../generated/l10n.dart';

class SettingsCubit extends Cubit<SettingsPageInitialState> {
  SettingsCubit() : super(SettingsPageInitialState());

  void updateLanguageRu() {
    eventBus.fire(UpdateLocaleEventRu());
  }

  void updateLanguageEn() {
    eventBus.fire(UpdateLocaleEventEn());
  }
}
