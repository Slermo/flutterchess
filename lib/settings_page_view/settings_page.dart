import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chess/main.dart';
import 'package:flutter_chess/settings_page_view/settings_cubit.dart';
import 'package:flutter_chess/settings_page_view/settings_state.dart';

import '../features/event_bus.dart';
import '../generated/l10n.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    //final SettingsCubit settingsCubit = context.cubit<SettingsCubit>;
    return BlocProvider<SettingsCubit>(
      create: (context) => SettingsCubit(),
      child: BlocBuilder<SettingsCubit, SettingsPageInitialState>(
        builder: (context, state) {
          return Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Color.fromARGB(255, 231, 171, 121),
                  Color.fromARGB(255, 178, 80, 104),
                  Color.fromARGB(255, 76, 58, 81),
                ],
              ),
            ),
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.wysiwyg_sharp,
                        color: Colors.white,
                        size: 52,
                      ),
                      RichText(
                        text: TextSpan(
                          text: S.of(context).settings_page__title,
                          style: const TextStyle(
                              fontFamily: 'BebasNeue',
                              fontSize: 35,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RichText(
                            text: TextSpan(
                              text: S.of(context).settings_page__language,
                              style: const TextStyle(
                                  fontFamily: 'BebasNeue',
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              BlocProvider.of<SettingsCubit>(context)
                                  .updateLanguageRu();
                              // eventBus.fire(UpdateLocaleEventRu());
                            },
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: Container(
                                decoration:
                                    BoxDecoration(border: Border.all(width: 1)),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              BlocProvider.of<SettingsCubit>(context)
                                  .updateLanguageEn();
                              //eventBus.fire(UpdateLocaleEventEn());
                            },
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: Container(
                                decoration:
                                    BoxDecoration(border: Border.all(width: 1)),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
