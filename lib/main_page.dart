import 'package:flutter/material.dart';
import 'package:flutter_chess/generated/l10n.dart';

import 'navbar.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromARGB(255, 76, 58, 81),
            Color.fromARGB(255, 178, 80, 104),
            Color.fromARGB(255, 231, 171, 121),
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Icon(
            Icons.wysiwyg_sharp,
            color: Colors.white,
            size: 52,
          ),
          RichText(
            text: TextSpan(
              text: S.of(context).main_page__name,
              style: const TextStyle(fontFamily: 'BebasNeue', fontSize: 35),
            ),
          ),
          RichText(
            text: TextSpan(
              text: S.of(context).main_page__description,
              style: const TextStyle(
                  color: Colors.grey, fontFamily: 'BebasNeue', fontSize: 20),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 20, 40, 20),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NavBar()),
                );
              },
              child: Container(
                height: 70,
                decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 178, 80, 104),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: FittedBox(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RichText(
                      text: TextSpan(
                        text: S.of(context).main_page__button,
                        style: const TextStyle(
                            color: Colors.white,
                            fontFamily: 'BebasNeue',
                            fontSize: 25),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
