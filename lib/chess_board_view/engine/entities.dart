import '../chess_enums.dart';

class TileInfo {
  final bool isBlack;
  final bool isClicked;
  final FigureAsset figure;

  TileInfo(
      {required this.isClicked, required this.figure, required this.isBlack});
}

class FigureAsset {
  final bool figureType; // есть ли вообще
  final String figureAsset;
  final bool color; // 0 - черный, 1 - белый
  FigureAsset(
      {required this.figureType,
      required this.figureAsset,
      required this.color});
}
