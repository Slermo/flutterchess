part of 'chess_page_bloc.dart';

@immutable
abstract class ChessPageEvent {}

class UpdateChessBoardEvent extends ChessPageEvent {}
