import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../Logica/chess_board_class.dart';
import '../chess_board_widget.dart';
import 'chess_page_bloc.dart';

class ChessPage extends StatelessWidget {
  const ChessPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ChessPageBloc()..add(UpdateChessBoardEvent()),
      child: BlocBuilder<ChessPageBloc, ChessPageInitialState>(
        builder: (context, state) {
          return Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Color.fromARGB(255, 76, 58, 81),
                  Color.fromARGB(255, 178, 80, 104),
                  Color.fromARGB(255, 231, 171, 121),
                ],
              ),
            ),
            child: SafeArea(
              child: Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: ChessBoardWidget(
                          boardFigures: state.boardFigures,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
