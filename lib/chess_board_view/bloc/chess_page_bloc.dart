import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_chess/Logica/chess_board_class.dart';
import 'package:meta/meta.dart';

import '../chess_enums.dart';
import '../engine/entities.dart';

part 'chess_page_event.dart';
part 'chess_page_state.dart';

class ChessPageBloc extends Bloc<ChessPageEvent, ChessPageInitialState> {
  ChessPageBloc() : super(ChessPageInitialState()) {
    on<UpdateChessBoardEvent>(_updateChessBoardEvent);
  }
  ChessBoard board = ChessBoard();
  FutureOr<void> _updateChessBoardEvent(
      UpdateChessBoardEvent event, Emitter<ChessPageInitialState> emit) {
    board.click(4, 4);
    List<List<int>> currentBoard = board.arr;
    List<List<int>> clickableBoard = board.movePlates;
    List<List<TileInfo>> tiles = List.generate(8, (i) {
      return List.generate(8, (k) {
        return TileInfo(
            figure: FigureAsset(
                figureAsset: assetsOfFigures[currentBoard[i][k]] ?? '',
                figureType: true,
                color: board.getColor(i, k)),
            isBlack: (i + k) % 2 == 0 ? true : false,
            isClicked: clickableBoard[i][k] == 1 ? true : false);
      });
    });
    print(tiles[0][0].figure.figureAsset);
    emit(state.copyWith(boardFigures: tiles));
  }
}
