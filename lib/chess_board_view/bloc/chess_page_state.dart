part of 'chess_page_bloc.dart';

class ChessPageInitialState {
  final List<List<TileInfo>>? boardFigures;

  ChessPageInitialState({this.boardFigures});

  ChessPageInitialState copyWith({final List<List<TileInfo>>? boardFigures}) {
    return ChessPageInitialState(
        boardFigures: boardFigures ?? this.boardFigures);
  }
}
