enum Figures { none, paw, rook, knight, bishop, queen, king }

final Map<int, String> assetsOfFigures = {
  0: 'assets/none.png',
  1: 'assets/paw.png',
  2: 'assets/rook.png',
  3: 'assets/knight.png',
  4: 'assets/bishop.png',
  5: 'assets/queen.png',
  6: 'assets/king.png',
};
