import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../Logica/chess_board_class.dart';
import 'chess_enums.dart';
import 'chess_tile_widget.dart';
import 'engine/entities.dart';

class ChessBoardWidget extends StatefulWidget {
  final List<List<TileInfo>>? boardFigures;
  const ChessBoardWidget({super.key, required this.boardFigures});

  @override
  State<ChessBoardWidget> createState() => _ChessBoardWidgetState();
}

class _ChessBoardWidgetState extends State<ChessBoardWidget> {
  ChessBoard board = ChessBoard();
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        for (int i = 0; i < 8; i++) ...[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              for (int k = 0; k < 8; k++) ...[
                if (widget.boardFigures != null)
                  ChessTile(
                      tile: widget.boardFigures?[i][k] ??
                          TileInfo(
                              isClicked: false,
                              figure: FigureAsset(
                                figureAsset: 'assets/none.png',
                                figureType: false,
                                color: board.getColor(i, k),
                              ),
                              isBlack: (i + k) % 2 == 0 ? true : false))
              ]
            ],
          ),
        ]
      ],
    );
  }
}
