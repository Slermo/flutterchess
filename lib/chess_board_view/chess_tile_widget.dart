import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/chess_page_bloc.dart';
import 'chess_enums.dart';
import 'engine/entities.dart';

class ChessTile extends StatelessWidget {
  final TileInfo tile;
  const ChessTile({super.key, required this.tile});

  @override
  Widget build(BuildContext context) {
    ChessPageBloc _bloc = ChessPageBloc();
    return BlocProvider(
      create: (context) => ChessPageBloc(),
      child: BlocBuilder<ChessPageBloc, ChessPageInitialState>(
        builder: (context, state) {
          return GestureDetector(
            onTap: () {
              _bloc.add(UpdateChessBoardEvent());
            },
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: tile.isBlack ? Colors.teal : Colors.orange,
                    image: tile.figure.figureType
                        ? DecorationImage(
                            image: AssetImage(tile.figure.figureAsset),
                          )
                        : const DecorationImage(
                            image: AssetImage('assets/none.png'))),
                child: Image.asset(tile.isClicked
                    ? 'assets/green_dot.png'
                    : 'assets/none.png'),
              ),
            ),
          );
        },
      ),
    );
  }
}
