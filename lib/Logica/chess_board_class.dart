import 'dart:io';

import 'chess_class.dart';

class ChessBoard {
  bool isChoosed = false;
  int x = -1, y = -1; //координаты выбранной в данный момент фигуры
  ChessBoard() {
    resetMovePlates();
    update();
  }
  List<List<Chess>> _board = [
    // массив фигур
    [
      new Chess(2, false),
      new Chess(3, false),
      new Chess(4, false),
      new Chess(5, false),
      new Chess(6, false),
      new Chess(4, false),
      new Chess(3, false),
      new Chess(2, false)
    ],
    [
      new Chess(1, false),
      new Chess(1, false),
      new Chess(1, false),
      new Chess(1, false),
      new Chess(1, false),
      new Chess(1, false),
      new Chess(1, false),
      new Chess(1, false)
    ],
    [
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true)
    ],
    [
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true)
    ],
    [
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(5, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true)
    ],
    [
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true),
      new Chess(0, true)
    ],
    [
      new Chess(1, true),
      new Chess(1, true),
      new Chess(1, true),
      new Chess(1, true),
      new Chess(1, true),
      new Chess(1, true),
      new Chess(1, true),
      new Chess(1, true)
    ],
    [
      new Chess(2, true),
      new Chess(3, true),
      new Chess(4, true),
      new Chess(5, true),
      new Chess(6, true),
      new Chess(4, true),
      new Chess(3, true),
      new Chess(2, true)
    ],
  ];

  List<List<int>> arr = [
    // массив для вывода в консоль
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0]
  ];
  List<List<int>> movePlates = [
    //массив полей, в которые может сходить выбранная фигура
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
  ];

  void resetMovePlates() {
    //сбросс массива ходов
    movePlates = [
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0]
    ];
    isChoosed = false;
    x = -1;
    y = -1;
  }

  void update() {
    //функция обновления массива для вывода
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        arr[i][j] = _board[i][j].type;
      }
    }
  }

  bool getColor(int i, int j) {
    return _board[i][j].isWhite;
  }

  void outputModified() {
    print("\n\n");
    for (int i = 0; i < 8; i++) {
      stdout.write('[');
      for (int j = 0; j < 8; j++) {
        stdout.write(arr[i][j]);
        stdout.write(', ');
      }
      stdout.write(']   [');
      for (int j = 0; j < 8; j++) {
        stdout.write(movePlates[i][j]);
        stdout.write(', ');
      }
      stdout.write(']\n');
    }
    // print('\n is choosed = $isChoosed');
    // print('x = $x');
    // print('y = $y');
  }

  void output() {
    print("\n\n");
    for (int i = 0; i < 8; i++) {
      stdout.write('[');
      for (int j = 0; j < 8; j++) {
        stdout.write(arr[i][j]);
        stdout.write(', ');
      }
      stdout.write(']');
    }
    for (int i = 0; i < 8; i++) {
      print(arr[i]);
    }
  }

  void outputMovePlates() {
    print("\n\n");
    for (int i = 0; i < 8; i++) {
      print(movePlates[i]);
    }
  }

  void chekMovePlates(int i, int j) {
    resetMovePlates();
    isChoosed = true;
    x = i;
    y = j;
    int n = _board[i][j].type;

    switch (n) {
      case 0:
        resetMovePlates();
        break;
      case 1:
        {
          if (_board[i][j].moveCounter != 0) {
            if (_board[i][j].isWhite == true) {
              if (i - 1 > 0 && _board[i - 1][j].type == 0) {
                movePlates[i - 1][j] = 1;
              } else {
                if (i - 1 == 0) {
                  //место для замены пешки
                }
                print('gg');
                break;
              }
            } else {
              //место для чёрных
              if (i + 1 < 7 && _board[i + 1][j].type == 0) {
                movePlates[i - 1][j] = 1;
              } else {
                if (i + 1 == 7) {
                  //место для замены пешки
                }
                print('gg');
                break;
              }
              break;
            }
            break;
          } else {
            if (_board[i][j].isWhite == true) {
              if (_board[i - 1][j].type == 0) {
                movePlates[i - 1][j] = 1;
                if (_board[i - 2][j].type == 0) {
                  movePlates[i - 2][j] = 1;
                } else {
                  print('gg');
                  break;
                }
              } else {
                print('gg');
                break;
              }
            } else {
              //место для чёрных
              if (_board[i + 1][j].type == 0) {
                movePlates[i + 1][j] = 1;
                if (_board[i + 2][j].type == 0) {
                  movePlates[i + 2][j] = 1;
                } else {
                  print('gg');
                  break;
                }
              } else {
                print('gg');
                break;
              }
            }
          }
        }
        break;
      case 2:
        {
          int a = i, b = j + 1;

          while (b < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;

            b++;
          }

          b = j - 1;

          while (b >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;

            b--;
          }
          a = i - 1;
          b = j;

          while (a >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a--;
          }
          a = i + 1;

          while (a < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a++;
          }
        }
        break;
      case 3:
        {
          int a = i - 1, b = j + 2;
          if (a >= 0 && b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
          a--;
          b--;
          if (a >= 0 && b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b -= 2;
          if (a >= 0 && b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b--;
          a++;
          if (a >= 0 && b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          a += 2;
          if (a < 8 && b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          a++;
          b++;
          if (a < 8 && b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b += 2;
          if (a < 8 && b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b++;
          a--;
          if (a < 8 && b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
        }
        break;
      case 4:
        {
          int a = i + 1, b = j + 1;

          while (a < 8 && b < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a++;
            b++;
          }
          a = i - 1;
          b = j - 1;

          while (a >= 0 && b >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a--;
            b--;
          }
          a = i - 1;
          b = j + 1;

          while (a >= 0 && b < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a--;
            b++;
          }
          a = i + 1;
          b = j - 1;

          while (a < 8 && b >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a++;
            b--;
          }
        }
        break;
      case 5:
        {
          int a = i, b = j + 1;

          while (b < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;

            b++;
          }

          b = j - 1;

          while (b >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;

            b--;
          }
          a = i - 1;
          b = j;

          while (a >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a--;
          }
          a = i + 1;

          while (a < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a++;
          }
          a = i + 1;
          b = j + 1;
          while (a < 8 && b < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a++;
            b++;
          }
          a = i - 1;
          b = j - 1;

          while (a >= 0 && b >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a--;
            b--;
          }
          a = i - 1;
          b = j + 1;

          while (a >= 0 && b < 8 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a--;
            b++;
          }
          a = i + 1;
          b = j - 1;

          while (a < 8 && b >= 0 && _board[a][b].type == 0) {
            movePlates[a][b] = 1;
            a++;
            b--;
          }
        }
        break;
      case 6:
        {
          int a = i + 1, b = j + 1;
          if (a < 8 && b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b--;
          if (a < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b--;
          if (a < 8 && b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          a--;
          if (b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          a--;
          if (a >= 0 && b >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b++;
          if (a >= 0 && _board[a][b].type == 0) movePlates[a][b] = 1;
          b++;
          if (a >= 0 && b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
          a++;
          if (b < 8 && _board[a][b].type == 0) movePlates[a][b] = 1;
        }
        break;

      default:
        print("No such chess!!!");
    }
  }

  void move(/*to->*/ int i, int j) {
    if (x != -1 && y != -1) {
      if (movePlates[i][j] == 1) {
        _board[x][y].moveCounter++;
        Chess c = _board[i][j];
        _board[i][j] = _board[x][y];
        _board[x][y] = c;
        print('xy =');
        print(_board[x][y].moveCounter);
        print('ij =');
        print(_board[i][j].moveCounter);
      }
    } else {
      print('X or Y == -1 !!!');
    }
    print('normas');
    resetMovePlates();
    update();
  }

  void click(int i, int j) {
    if (isChoosed) {
      move(i, j);
    } else {
      chekMovePlates(i, j);
    }
  }
}
