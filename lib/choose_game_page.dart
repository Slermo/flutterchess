import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_chess/generated/l10n.dart';

import 'chess_board_view/bloc/chess_page_view.dart';

class GamePicker extends StatelessWidget {
  const GamePicker({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromARGB(255, 231, 171, 121),
            Color.fromARGB(255, 178, 80, 104),
            Color.fromARGB(255, 76, 58, 81),
          ],
        ),
      ),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.wysiwyg_sharp,
                  color: Colors.white,
                  size: 52,
                ),
                RichText(
                  text: TextSpan(
                    text: S.of(context).main_page__name,
                    style:
                        const TextStyle(fontFamily: 'BebasNeue', fontSize: 35),
                  ),
                ),
              ],
            )),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const ChessPage()),
                      );
                    },
                    icon: const Icon(
                      Icons.new_label_outlined,
                      color: Colors.white,
                    ),
                    style: ElevatedButton.styleFrom(
                      side: const BorderSide(width: 2, color: Colors.white60),
                      backgroundColor: const Color.fromARGB(255, 177, 102, 40),
                      shape: const RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(20)),
                      ),
                    ),
                    label: RichText(
                      text: TextSpan(
                        text: S.of(context).picker_page__start,
                        style: const TextStyle(
                            fontFamily: 'BebasNeue', fontSize: 35),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
