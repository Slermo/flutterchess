import 'package:flutter/material.dart';
import 'package:flutter_chess/Logica/chess_board_class.dart';
import 'package:flutter_chess/Logica/chess_class.dart';
import 'package:flutter_chess/main_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'features/event_bus.dart';
import 'generated/l10n.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LocaleWidget();
  }
}

class LocaleWidget extends StatefulWidget {
  const LocaleWidget({super.key});

  @override
  State<LocaleWidget> createState() => _LocaleWidgetState();
}

class _LocaleWidgetState extends State<LocaleWidget> {
  Locale locale = const Locale('en', '');

  @override
  void initState() {
    super.initState();
    eventBus.on<UpdateLocaleEventRu>().listen((event) {
      setState(() {
        locale = const Locale('ru', '');
      });
    });

    eventBus.on<UpdateLocaleEventEn>().listen((event) {
      setState(() {
        locale = const Locale('en', '');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: locale,
      supportedLocales: const [
        Locale('en', ''),
        Locale('ru', ''),
      ],
      home: MainPage(),
    );
  }
}
